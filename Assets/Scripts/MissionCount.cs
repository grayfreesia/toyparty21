﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MissionCount : MonoBehaviour
{
    public int[] counts;
    public Text[] texts;
    public int done;
    public static MissionCount Instance{get; private set;}

    void Awake()
    {
        if(Instance == null) Instance = this;
    }

    public void CountDown(int num)
    {
        if(num >= counts.Length) return;
        if(counts[num]-- > 0)
        {
            texts[num].text = counts[num].ToString();
        }

        if(counts[num] == 0)
        {
            //Clear
            texts[num].text ="v";
            texts[num].color = Color.green;
            done ++;
        }

        if(done == counts.Length)
        {
            // Debug.Log("Clear level!");
            StartCoroutine(TweenText.Instance.Tweening("<color=orange>Level Clear!</color>", false));
        }
    }
}
