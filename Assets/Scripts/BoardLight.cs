﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardLight : MonoBehaviour
{
    SpriteRenderer sr;
    bool flag;
    float interval = .05f;
    float min = .5f;
    void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
        
    }

    public void OnEnable()
    {
        StartCoroutine(Light());
    }

    // Update is called once per frame
    IEnumerator Light()
    {
        while(true)
        {
            yield return new WaitForSeconds(.05f);
            if(flag)
            {
                sr.color += new Color(0f,0f,0f,interval);
                if(sr.color.a > .9f) flag = false;
            }
            else
            {
                sr.color -= new Color(0f,0f,0f,interval);
                if(sr.color.a <= min) flag = true;
            }
        }
        

    }
}
