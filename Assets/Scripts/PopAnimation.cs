﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopAnimation : MonoBehaviour
{
    public Color[] colors;
    
    public void SetColor(int num)
    {
        GetComponent<SpriteRenderer>().color = colors[num];
    }

    public void End()
    {
        Destroy(gameObject);
    }
}
