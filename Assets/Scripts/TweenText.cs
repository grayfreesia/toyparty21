﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TweenText : MonoBehaviour
{
    public Text text;
    public GameObject layer;
    public bool isEnd = false;
    public static TweenText Instance{get; private set;}

    void Awake()
    {
        if(Instance == null) Instance = this;
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.T))
        {
            StartCoroutine(Tweening("sssss", true));
            
        }
    }

    public IEnumerator Tweening(string s, bool upward)
    {
        if(!isEnd)
        {
            if(!upward)
            {              
                isEnd = true;
                BoardManager.Instance.hintTime = -10000f;
                layer.SetActive(true);
            }
            text.text = s;
            iTween.Stop(gameObject);
            transform.position = new Vector3(Screen.width *.5f, (upward?-0.5f:1.5f)*Screen.height, 0);
            iTween.MoveTo(gameObject, new Vector3(Screen.width *.5f, Screen.height * 0.6f, 0f), (upward?1f:2f));
            yield return new WaitForSeconds((upward?1f:2f));
            if(upward)
            {
                iTween.MoveTo(gameObject, new Vector3(Screen.width *.5f, (upward?1.5f:-0.5f) * Screen.height, 0), (upward?1f:2f));
                yield return new WaitForSeconds((upward?1f:2f));
            }
        }
    }
}
