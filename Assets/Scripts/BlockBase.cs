﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockBase : MonoBehaviour
{
    public bool isMovable;
    public bool isObstacle;
    public bool isSpecial;

    public int colorNum = -1;

    private bool isMoving = false;
    public bool done = true;
    //[SerializeField]
    public List<Vector3> path = new List<Vector3>();


    protected void Start()
    {
        ClearPath();
    }

    public virtual void SetColor(int num)
    {

    }

    public virtual void SetObstacleState()
    {
        
    }

    public void SetPath(Vector3 pos)
    {
        path.Add(pos);
        done = false;
    }

    public void ClearPath()
    {
        path.Clear();
        isMoving = false;
        done = true;

    }

    public void MovePath()
    {
        if(path.Count < 1 || isMoving) return;
        float time = .5f * Mathf.Sqrt(path.Count);
        path.Insert(0, transform.parent.transform.position);
        path.Sort(delegate(Vector3 a, Vector3 b){
            if(a.y < b.y) return 1;
            else if(a.y > b.y) return -1;
            else return 0;
        });
        iTween.MoveTo(gameObject, iTween.Hash("path", path.ToArray(), "time", time, "easetype", "easeOutBounce", "oncomplete", "ClearPath"));
        isMoving = true;
    }

}
