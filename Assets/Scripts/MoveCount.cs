﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoveCount : MonoBehaviour
{
    public Text text;
    private int count = 20;
    public static MoveCount Instance{get; private set;}
    
    void Awake()
    {
        if(Instance == null) Instance = this;
    }

    public void CountDown()
    {
        if(count-- > 0)
        {
            text.text = count.ToString();
        }
    }

    public void IsFail()
    {
        if(count <= 0)
        {
            //Fail
            Debug.Log("Fail");
            StartCoroutine(TweenText.Instance.Tweening("<color=red>Fail!</color>", false));

        }
    }

}
