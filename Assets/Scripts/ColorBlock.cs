﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorBlock : BlockBase
{
    public Sprite[] sprites;
    void Awake()
    {
        isMovable = true;
        isObstacle = false;
        isSpecial = false;       
    }

    public override void SetColor(int num)
    {
        colorNum = num;
        GetComponent<SpriteRenderer>().sprite = sprites[colorNum];
    }



}
