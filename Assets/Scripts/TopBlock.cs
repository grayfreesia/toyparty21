﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopBlock : BlockBase
{
    public int state = 0;
    void Awake()
    {
        isMovable = true;
        isObstacle = true;
        isSpecial = false;  

    }

    public override void SetObstacleState()
    {
        state++;
        if(state == 1) 
        {
            GetComponent<Animator>().SetBool("state",true);
        }
        else if(state == 2)
        {
            //End();
            GetComponent<SpriteRenderer>().sortingOrder = 3;
            transform.parent.GetComponent<BoardTile>().block = null;
            transform.parent = BoardManager.Instance.transform;
            iTween.MoveTo(gameObject, iTween.Hash("position", transform.parent.transform.position ,"easetype", iTween.EaseType.easeInBack,
            "oncomplete","End", "time", 1f));
        }     
    }

    private void End()
    {
        Destroy(gameObject);
        MissionCount.Instance.CountDown(0);
        Point.Instance.AddPoint(500);
    }
    
}
