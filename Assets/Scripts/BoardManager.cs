﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


public class BoardManager : MonoBehaviour
{
    public GameObject boardTilePrefab;
    public GameObject colorBlockPrefab;
    public GameObject topBlockPrefab;
    public GameObject popAniPrefab;
    public BoardTile[,] board;
    private int[,] boardInitData = new int[,]{
                                    {0, 0, 0, 1, 0, 0, 0},
                                    {0, 2, 2, 1, 2, 2, 0},
                                    {1, 1, 1, 1, 1, 1, 1},
                                    {1, 1, 1, 2, 1, 1, 1},
                                    {1, 2, 1, 1, 1, 2, 1},
                                    {0, 0, 2, 2, 2, 0, 0}};
    // private int[,] boardInitData = new int[,]{//test
    //                                 {0, 0, 0, 0, 1, 0, 0, 0},
    //                                 {0, 0, 2, 2, 1, 2, 2, 0},
    //                                 {0, 1, 1, 1, 1, 1, 1, 1},
    //                                 {0, 1, 1, 1, 2, 1, 1, 1},
    //                                 {0, 1, 2, 1, 1, 1, 2, 1},
    //                                 {0, 0, 0, 2, 2, 2, 0, 0}};
    // private int[,] boardInitData = new int[,]{//test
    //                                 {0, 1, 0, 0, 0, 1, 0, 0, 0},
    //                                 {0, 1, 0, 2, 2, 1, 2, 2, 0},
    //                                 {0, 1, 1, 1, 1, 1, 1, 1, 1},
    //                                 {0, 1, 1, 1, 1, 2, 1, 1, 1},
    //                                 {0, 1, 1, 2, 1, 1, 1, 2, 1},
    //                                 {0, 1, 0, 0, 2, 2, 2, 0, 0}};

    private int sizeX;
    private int sizeY;

    private List<BoardTile> canPop = new List<BoardTile>();
    private BoardTile canMove;
    private BoardTile toMove;

    public float hintTime = 0f;
    //private bool hintFlag = true;

    public int comboCount = 1;

    public static BoardManager Instance{get; private set;}
    void Awake()
    {
        if(Instance == null) Instance = this;
    }

    void Start()
    {  
        SetBoard();
    }

    public int sideCount = 0;
    public int popDelay = 0;
    void Update()
    {
        if(DropBlock()) MovePathAllBlock();
        if(IsDropDone())
        {
            BoardTile.turn = true;
            
            if(BoardTile.popList.Count > 0 && !BoardTile.isSwapping)
            {
                BoardTile.turn = false;

                if(popDelay++ > 10)
                {
                    popDelay = 0;
                    List<BoardTile> list = BoardTile.PopMatch(BoardTile.popList.Distinct().ToList());
                    BoardTile.popList.Clear();
                    if(list.Count == 0)
                    {
                        MoveCount.Instance.IsFail();
                        comboCount = 1;
                        bool c = CanMove();
                        Debug.Log("can move ? : "+c);
                        if(!c)
                        {
                            Shuffle();
                        }
                    }
                    else
                    {
                        AddComboCount();
                    }
                }
            }
        } 
        else
        {
            BoardTile.turn = false;
        }  

        if(BoardTile.turn)
        {
            hintTime += Time.deltaTime;
            if(hintTime > 5f)
            {
                ShowHint();
                hintTime = 0f;
            }
        }
    }

    
    private void SetBoard()
    {
        float tileSize = 2.3f;
        float sizeDivider = 2.3f;

        float hexHeight = 2f;
        float hexWidth = 1.732f;
        
        sizeX = boardInitData.GetLength(1);
        sizeY = boardInitData.GetLength(0);
        sizeDivider /= ((sizeX > sizeY)? sizeX : sizeY);
        float startX = -(sizeX - 1)/2f;
        float startY = (sizeY - 1)/2f;
        board = new BoardTile[sizeX, sizeY];

        //create board
        for(int i = 0; i < sizeY; i++)
        {
            for(int j = 0; j < sizeX; j++)
            {
                if(boardInitData[i,j] > 0)
                {
                    Vector3 pos = new Vector3((startX+j)*hexWidth, (startY-i+((j%2==0)?0.5f:0f))*hexHeight, 0f)*sizeDivider;
                    GameObject tile = Instantiate(boardTilePrefab, pos, Quaternion.identity);
                    board[j, i] = tile.GetComponent<BoardTile>();
                    board[j, i].SetPos(j, i);                   
                    tile.transform.localScale = Vector3.one*tileSize*sizeDivider;
                    iTween.ScaleFrom(tile.gameObject, Vector3.zero, 1f);
                    if(boardInitData[i,j] == 1)//color blcok
                    {
                        int c = Random.Range(0,6);
                        for(int k = 0; k < 6; k++)//to prevent matched block at first time
                        {
                            if(CheckMatch(j, i, c).Count > 0) c = (c+1) % 6;                           
                            else break;
                        }
                        
                        board[j, i].InitColorBlock(colorBlockPrefab, c);
                    }
                    else if(boardInitData[i,j] == 2)//spinning-top block
                    {
                        board[j, i].InitTopBlock(topBlockPrefab);

                    }//...
                }
            }
        }
        //set spawner
        board[3,0].isSpawner = true;

        if(!CanMove()) Shuffle();

    }

    public List<BoardTile> CheckMatch(int x, int y, int color)
    {
        List<BoardTile> list = new List<BoardTile>();//to delete
        List<BoardTile> temp = new List<BoardTile>();
        int matchLines = 0;
        //line-match check
        //direction : |
        if(GetBlockColor(x, y-1) == color && color > -1)//same color with 1 above block
        {
            temp.Add(board[x, y-1]);
            if(GetBlockColor(x, y-2) == color && color > -1)//same color with 2 above block
            {
                temp.Add(board[x, y-2]);//
            }
        }
        if(GetBlockColor(x, y+1) == color && color > -1)//same color with 1 below block
        {
            temp.Add(board[x, y+1]);
            if(GetBlockColor(x, y+2) == color && color > -1)//same color 2 below block
            {
                temp.Add(board[x, y+2]);
            }
        }
        if(temp.Count >= 2)
        {
            //Debug.Log(x+" "+y+" match|");
            matchLines++;
            list.AddRange(temp);
        }
        //if temp count >= 3 : rocket
        //if temp count >= 4 : ufo
        temp.Clear();

        //direction : \
        if(GetBlockColor(x-1, (x%2==0)?y-1:y) == color && color > -1)//same color with 1 left-above block
        {
            temp.Add(board[x-1, (x%2==0)?y-1:y]);
            if(GetBlockColor(x-2, y-1) == color && color > -1)//same color with 2 left-above block
            {
                temp.Add(board[x-2, y-1]);
            }
        }
        if(GetBlockColor(x+1, (x%2==0)?y:y+1) == color && color > -1)//same color with 1 right-below block
        {
            temp.Add(board[x+1, (x%2==0)?y:y+1]);
            if(GetBlockColor(x+2, y+1) == color && color > -1)//same color 2 right-below block
            {
                temp.Add(board[x+2, y+1]);
            }
        }
        if(temp.Count >= 2)
        {
            //Debug.Log(x+" "+y+" match\\");

            matchLines++;
            list.AddRange(temp);
        }
        //if temp count >= 3 : rocket
        //if temp count >= 4 : ufo
        temp.Clear();

        //direction : /
        if(GetBlockColor(x+1, (x%2==0)?y-1:y) == color && color > -1)//same color with 1 right-above block
        {
            temp.Add(board[x+1, (x%2==0)?y-1:y]);
            if(GetBlockColor(x+2, y-1) == color && color > -1)//same color with 2 right-above block
            {
                temp.Add(board[x+2, y-1]);
            }
        }
        if(GetBlockColor(x-1, (x%2==0)?y:y+1) == color && color > -1)//same color with 1 left-below block
        {
            temp.Add(board[x-1, (x%2==0)?y:y+1]);
            if(GetBlockColor(x-2, y+1) == color && color > -1)//same color 2 left-below block
            {
                temp.Add(board[x-2, y+1]);
            }
        }
        if(temp.Count >= 2)
        {
            //Debug.Log(x+" "+y+" match/");

            matchLines++;
            list.AddRange(temp);
        }
        //if temp count >= 3 : rocket
        //if temp count >= 4 : ufo
        temp.Clear();

        if(matchLines > 0)
        {
            list.Add(board[x, y]);
        }

        return list;
    }

    private int GetBlockColor(int x, int y)
    {
        if(x < 0 || x >= sizeX || y < 0 || y >= sizeY || boardInitData[y, x] == 0 || board[x, y] == null || board[x, y].block == null) return -1;
        else return board[x, y].GetBlockColor();
    }

    

    public bool DropBlock()
    {
        bool down = true;
        bool side = true;
        //drop straight
        for(int i = sizeY-1; i >= 0; i--)//int i = 0; i < sizeY; i++)//
        {
            //yield return new WaitForEndOfFrame();
            //odd :little lower
            for(int j = 1; j < sizeX; j += 2)
            {               
                if(board[j, i]!= null)
                {                   
                    if(board[j, i].DropDownward()) 
                    {
                        down = false;
                    }
                }
            }   
            //yield return new WaitForEndOfFrame();
            //even :little upper
            for(int j = 0; j < sizeX; j += 2)
            {
                if(board[j, i]!= null)
                {                   
                    if(board[j, i].DropDownward()) 
                    {
                        down = false;
                    }
                }
            }      
                            
        }
        if(!down) return false;
        if(sideCount++ < -1) return false;
        sideCount = 0;
        //drop from side
        for(int i = 0; i < sizeY; i++)//int i = sizeY-1; i >= 0; i--)//
        {
            //even :little upper
            for(int j = 0; j < sizeX; j += 2)
            {
                if(board[j, i]!= null)
                {                   
                    if(board[j, i].DropSide()) 
                    {
                        side = false;
                        return false;
                    }
                }
            }   
            //odd :little lower
            for(int j = 1; j < sizeX; j += 2)
            {
                if(board[j, i]!= null)
                {                   
                    if(board[j, i].DropSide()) 
                    {
                        side = false;
                        return false;
                    }
                }
            }  
                 

                                
        }
        if(!side) return false;
        else return true;

    }

    private void MovePathAllBlock()
    {

        for(int i = sizeY-1; i >= 0; i--)
        {
            //odd :little lower
            for(int j = 1; j < sizeX; j += 2)
            {
                if(board[j, i]!= null && board[j,i].block != null)
                {
                    board[j,i].block.MovePath();
                }
            }     
                       
            //even :little upper
            for(int j = 0; j < sizeX; j += 2)
            {

                if(board[j, i]!= null && board[j,i].block != null)
                {
                    board[j,i].block.MovePath();

                }
            } 

        }

    }

    private bool IsDropDone()
    {
        bool d = true;
        for(int i = 0; i < sizeY; i++)
        {
            for(int j = 0; j < sizeX; j++)
            {
                if(board[j, i] == null || board[j, i].block == null) continue;
                if(!board[j, i].block.done) d = false;
            }
        }
        return d;
    }

    private bool CanMove()
    {
        bool c = false;
        List<BoardTile> list = new List<BoardTile>();
        int colorNum;
        int[,] index;

        for(int i = 0; i < sizeY; i++)
        {
            for(int j = 0; j < sizeX; j++)
            {
                if(board[j, i] == null || board[j, i].block == null) continue;
                colorNum = board[j, i].block.colorNum;
                index = new int[6, 2]{{j,i+1}, {j,i-1}, {j+1,i}, {j-1,i}, {j+1,(j%2==0)?i-1:i+1}, {j-1,(j%2==0)?i-1:i+1}};
                for(int k = 0; k < 6; k++)
                {
                    if(index[k,0] < 0 || index[k,0] >= sizeX || index[k,1] < 0 || index[k,1] >= sizeY) continue;
                    if(board[index[k,0],index[k,1]]== null || board[index[k,0],index[k,1]].block == null) continue;
                    if(!board[j, i].block.isMovable || !board[index[k,0],index[k,1]].block.isMovable) continue;
                    if(colorNum != board[index[k,0],index[k,1]].block.colorNum)
                    {
                        int temp = board[j, i].block.colorNum;
                        board[j, i].block.colorNum = - 1;
                        list = CheckMatch(index[k,0],index[k,1], colorNum);
                        board[j, i].block.colorNum = temp;
                        if(list.Count > 0) 
                        {
                            canPop = list;
                            canMove = board[j, i];
                            toMove = board[index[k,0],index[k,1]];
                            //Debug.Log("can"+j+" "+i+" to "+index[k,0]+" "+index[k,1]+ " color "+colorNum);
                            return true;
                        }
                    }
                }               
            }
        }
        return c;
    }

    private void Shuffle()
    {
        //Debug.Log("Shuffle time!");
        StartCoroutine(TweenText.Instance.Tweening("<color=orange>Shuffle Time!</color>", true));
        List<BoardTile> tiles = new List<BoardTile>();
        for(int i = 0; i < sizeY; i++)
        {
            for(int j = 0; j < sizeX; j++)
            {
                if(board[j, i] != null && board[j, i].block != null && board[j, i].block.colorNum > -1)
                {
                    tiles.Add(board[j, i]);
                }
            }
        }
        for(int i = 0; i < tiles.Count; i++)
        {
            Destroy(tiles[i].block.gameObject);
            int c = Random.Range(0,6);
            for(int k = 0; k < 6; k++)//to prevent matched block at first time
            {
                if(CheckMatch(tiles[i].x, tiles[i].y, c).Count > 0) c = (c+1) % 6;                           
                else break;
            }
            tiles[i].InitColorBlock(colorBlockPrefab, c);
            iTween.ScaleFrom(tiles[i].block.gameObject, Vector3.zero, .5f);
        }

        if(!CanMove()) Shuffle();
    }

    private void ShowHint()
    {
        hintTime = 0;    
        iTween.MoveBy(canMove.block.gameObject, iTween.Hash("amount", (toMove.transform.position - canMove.transform.position)*0.2f, "looptype", iTween.LoopType.pingPong, "time", .5f));
        foreach(BoardTile b in canPop)
        {
            b.SetLight(true);
        }
    }


    public void OffHint()
    {
        iTween.Stop(canMove.block.gameObject);
        foreach(BoardTile b in canPop)
        {
            b.SetLight(false);
        }
        canMove.block.transform.localPosition = Vector3.zero;
        hintTime = 0;
    }

    private void AddComboCount()
    {
        comboCount++;
        switch(comboCount)
        {
            case 3:
                //Debug.Log("Fun!");
                StartCoroutine(TweenText.Instance.Tweening("<color=green>Fun!</color>", true));
                break;
            case 5:
                // Debug.Log("Toyrrific!");
                StartCoroutine(TweenText.Instance.Tweening("<color=red>Toyrrific!</color>", true));
                break;
            case 7:
                // Debug.Log("Toytastic!");
                StartCoroutine(TweenText.Instance.Tweening("<color=blue>Toytastic!</color>", true));
                break;
            ///
            ///
            ///
            default:
                break;
        }
    }

}
