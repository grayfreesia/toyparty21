﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class BoardTile : MonoBehaviour
{
    public BlockBase block;
    public int x, y;
    public bool isSpawner = false;

    public static BoardTile curTile = null;
    public static bool turn = true;
    
    int spawnCount = 0;
    public static int test = 0;
    public static bool isSwapping = false;
    public static List<BoardTile> popList = new List<BoardTile>();

    public GameObject lightPrefab;

    void Update()
    {
        SpawnColor();
    }

    public void InitColorBlock(GameObject colorBlockPrefab, int num)
    {
        block = Instantiate(colorBlockPrefab, transform).GetComponent<BlockBase>();
        block.SetColor(num);
    }

    public void InitTopBlock(GameObject topBlockPrefab)
    {
        block = Instantiate(topBlockPrefab, transform).GetComponent<BlockBase>();
    }

    public int GetBlockColor()
    {
        if(block == null) return -1;
        return block.colorNum;
    }

    public void SetPos(int ix, int iy)
    {
        x = ix;
        y = iy;
    }

    private void OnMouseDown()
    {
        if(turn)
        {
            curTile = this;
        }
    }

    private void OnMouseEnter()
    {
        if(Input.GetMouseButton(0) && curTile != null && turn && !TweenText.Instance.isEnd) 
        {
            
            SwapBlock();
        }

    }

    private void OnMouseUp()
    {
        curTile = null;
    }

    private void SwapBlock()
    {
        if(!IsNear(x, y, curTile.x, curTile.y))
        {
            //not near block
            Debug.Log("not near");
            return;
        }
        if(curTile.block == null || block == null || !curTile.block.isMovable || !block.isMovable) 
        {
            //cannot move
            Debug.Log("cannot move");

            return;
        }
        //swap
        if(!isSwapping) StartCoroutine(Swap());      
    }

    private bool IsNear(int x, int y, int cx, int cy)
    {
        if(x == cx)
        {
            if(y == cy + 1 || y == cy - 1) return true;
        }
        else if(x == cx - 1 || x == cx + 1)
        {
            if(y == cy || y == ((x%2==0)? cy+1 : cy-1)) return true;
        }      
        return false;
    }

    private IEnumerator Swap()
    {
        //Debug.Log("Swap");
        isSwapping = true;
        BoardManager.Instance.OffHint();
        BlockBase tempBlock = curTile.block;
        curTile.block = block;
        block = tempBlock;
        curTile.block.transform.parent = curTile.transform;
        block.transform.parent = transform;
        iTween.MoveTo(curTile.block.gameObject, curTile.transform.position, .5f);
        iTween.MoveTo(block.gameObject, transform.position, .5f);
        BoardTile temp = curTile;
        curTile = null;
        turn = false;
        yield return new WaitForSeconds(.5f);

        List<BoardTile> tiles = new List<BoardTile>();
        tiles.Add(this);
        tiles.Add(temp);
        List<BoardTile> list  = PopMatch(tiles);
        yield return new WaitForSeconds(.1f);

        if(list.Count <= 0)//reswap
        {
            curTile = temp;
            tempBlock = curTile.block;
            curTile.block = block;
            block = tempBlock;
            curTile.block.transform.parent = curTile.transform;
            block.transform.parent = transform;
            iTween.MoveTo(curTile.block.gameObject, curTile.transform.position, .5f);
            iTween.MoveTo(block.gameObject, transform.position, .5f);
            curTile = null;
            yield return new WaitForSeconds(.5f);
            turn = true;
        }
        else
        {
            //swap success
            MoveCount.Instance.CountDown();
        }

        isSwapping = false;
    }

    public static List<BoardTile> PopMatch(List<BoardTile> tiles)
    {
        List<BoardTile> list = new List<BoardTile>();
        foreach(BoardTile tile in tiles)
        {
            list.AddRange(BoardManager.Instance.CheckMatch(tile.x, tile.y, tile.block.colorNum));
        }
        list = list.Distinct().ToList();
        if(list.Count > 0)
        {
            for(int i = 0; i < list.Count; i++)
            {
                Instantiate(BoardManager.Instance.popAniPrefab, list[i].block.transform.parent).GetComponent<PopAnimation>().SetColor(list[i].block.colorNum);
                Destroy(list[i].block.gameObject);
                list[i].block = null;
                Point.Instance.AddPoint(20*BoardManager.Instance.comboCount);
            }
            PopNearObstacle(list);
            SoundManager.Instance.PlaySound(0);
        }
        return list;
    }

    private static void PopNearObstacle(List<BoardTile> list)
    {
        List<BoardTile> obstacles = new List<BoardTile>();
        BoardTile[,] board = BoardManager.Instance.board;
        int sizeX = board.GetLength(0);
        int sizeY = board.GetLength(1);
        for(int i = 0; i < list.Count; i++)
        {
            BoardTile cur = list[i];
            CheckObstacle(board, sizeX, sizeY, cur.x, cur.y+1, ref obstacles);
            CheckObstacle(board, sizeX, sizeY, cur.x, cur.y-1, ref obstacles);
            CheckObstacle(board, sizeX, sizeY, cur.x+1, cur.y, ref obstacles);
            CheckObstacle(board, sizeX, sizeY, cur.x-1, cur.y, ref obstacles);
            CheckObstacle(board, sizeX, sizeY, cur.x+1, (cur.x%2==0)?cur.y-1:cur.y+1, ref obstacles);
            CheckObstacle(board, sizeX, sizeY, cur.x-1, (cur.x%2==0)?cur.y-1:cur.y+1, ref obstacles);
        }
        obstacles = obstacles.Distinct().ToList();
        obstacles = obstacles.Except(list).ToList();

        for(int i = 0; i < obstacles.Count; i++)
        {
            obstacles[i].block.GetComponent<BlockBase>().SetObstacleState();
        }
        if(obstacles.Count > 0) SoundManager.Instance.PlaySound(1, .4f);

    }

    private static void CheckObstacle(BoardTile[,] board, int sizeX, int sizeY, int x, int y, ref List<BoardTile> obstacles)
    {
        if(x < 0 || x >= sizeX || y < 0 || y >= sizeY) return;
        if(board[x, y] == null||board[x, y].block == null) return;
        if(board[x, y].block.isObstacle) obstacles.Add(board[x, y]);
    }

    public bool DropDownward()
    {
        
        if(block == null) return false;
        if(!block.isMovable) return false;
        BoardTile[,] board = BoardManager.Instance.board;
        int sizeX = board.GetLength(0);
        int sizeY = board.GetLength(1);
        
        if(y + 1 < sizeY && board[x, y + 1] != null && board[x, y + 1].block == null)
        {
            board[x,y].block.SetPath(board[x,y+1].transform.position);
            
            board[x,y+1].block = board[x,y].block;
            board[x,y].block = null;
            board[x,y+1].block.transform.parent = board[x,y+1].transform;
            popList.Add(board[x,y+1]);
            return true;
        }
        return false;
    }

    public bool DropSide()
    {
        
        if(block == null) return false;
        if(!block.isMovable) return false;
        BoardTile[,] board = BoardManager.Instance.board;
        int sizeX = board.GetLength(0);
        int sizeY = board.GetLength(1);
        int c = x%2==1?1:0;

        //if(y - 1 >= 0 && board[x, y-1] != null && board[x, y-1].block != null && board[x, y-1].block.isMovable) return false;//when upper block exist, dont drop to side

        int nx = x-1;//left
        
        if(y + c < sizeY && nx >= 0 && nx < sizeX && board[nx, y + c] != null && board[nx, y + c].block == null)
        {
            if(y + c - 1 >= 0 && (board[nx, y + c - 1] == null || board[nx, y + c - 1].block == null || !board[nx, y + c - 1].block.isMovable))
            {
                board[x,y].block.SetPath(board[nx,y+c].transform.position);

                board[nx,y+c].block = board[x,y].block;
                board[x,y].block = null;
                board[nx,y+c].block.transform.parent = board[nx,y+c].transform;
                popList.Add(board[nx,y+c]);
                return true;
            }
            
        }
        if(block == null) return true;
        nx = x+1;//right
        if(y + c < sizeY && nx >= 0 && nx < sizeX && board[nx, y + c] != null && board[nx, y + c].block == null)
        {
            if(y + c - 1 >= 0 && (board[nx, y + c - 1] == null || board[nx, y + c - 1].block == null || !board[nx, y + c - 1].block.isMovable))
            {
                board[x,y].block.SetPath(board[nx,y+c].transform.position);

                board[nx,y+c].block = board[x,y].block;
                board[x,y].block = null;
                board[nx,y+c].block.transform.parent = board[nx,y+c].transform;
                popList.Add(board[nx,y+c]);
                return true;
            }
        }
        return false;
    }

    public bool SpawnColor()
    {
        if(isSpawner && block == null && spawnCount++ > 5)
        {
            InitColorBlock(BoardManager.Instance.colorBlockPrefab, Random.Range(0, 6));
            block.transform.Translate(Vector3.up * .5f);
            iTween.MoveTo(block.gameObject, iTween.Hash("position", transform.position, "time", .1f, "easetype", "easeInQuad"));
            spawnCount = 0;
            popList.Add(this);

            return true;
        }
        return false;
    }

    public void SetLight(bool on)
    {
        lightPrefab.SetActive(on);
    }
}
