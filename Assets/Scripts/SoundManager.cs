﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    private AudioSource audioSource;
    public AudioClip[] clips;
    public static SoundManager Instance{get; private set;}

    void Awake()
    {
        if(Instance == null) Instance = this;
        audioSource = GetComponent<AudioSource>();
    }

    public void PlaySound(int num)
    {
        if(num >= clips.Length) return;
        audioSource.PlayOneShot(clips[num]);
    }

    public void PlaySound(int num, float volume)
    {
        if(num >= clips.Length) return;
        audioSource.PlayOneShot(clips[num], volume);
    }
}
