﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Point : MonoBehaviour
{
    public int point = 0;
    private Text text;
    public static Point Instance{get; private set;}

    void Awake()
    {
        if(Instance == null) Instance = this;
        text = GetComponent<Text>();
    }

    public void AddPoint(int num)
    {
        point += num;
        text.text = point.ToString("#,#");
    }
}
